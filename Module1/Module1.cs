﻿using System;
using System.Collections.Specialized;
using System.Linq.Expressions;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 Mod1 = new Module1();

            int a, b;

            Console.WriteLine("Input element a: ");
            a = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Input element b: ");
            b = Int32.Parse(Console.ReadLine());

            int[] firstArr = Mod1.SwapItems(a, b);

            for (int i = 0; i < firstArr.Length; i++)
            {
                Console.WriteLine("Next element is: {0}", firstArr[i]);
            }

            int c, d, e;

            Console.WriteLine("Input element c: ");
            c = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Input element d: ");
            d = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Input element e: ");
            e = Int32.Parse(Console.ReadLine());

            int[] secondArr = new int[3] { c, d, e };

            int minVal = Mod1.GetMinimumValue(secondArr);

            Console.WriteLine("Minimum element in array is: {0}", minVal);

        }


        public int[] SwapItems(int a, int b)
        {
            int tempInt;

            tempInt = a;

            a = b;

            b = tempInt;

            int[] arr = new int[2] { a, b };

            return arr;
        }

        public int GetMinimumValue(int[] input)
        {

            int minVal = 0;

            if (input.Length > 0)
            { 

                for (int i = 0; i < input.Length; i++)
                {
                    if (i == 0)
                    {
                        minVal = input[i];
                    }
                    else
                    {
                        if (minVal > input[i])
                        {
                            minVal = input[i];
                        }
                    }
                }
            }
            else
            {
                throw (new Exception("Array is empty!!!"));
            }

            return minVal;
        }
    }
}
